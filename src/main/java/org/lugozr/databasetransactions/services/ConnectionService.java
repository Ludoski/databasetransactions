package main.java.org.lugozr.databasetransactions.services;

import main.java.org.lugozr.databasetransactions.exceptions.UnableToConnectException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService{

    public Connection getConnection()throws UnableToConnectException {
        Connection connection;
        try{
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","password");
        } catch (SQLException e){
            throw new UnableToConnectException(e);
        }
        return connection;
    }
}
