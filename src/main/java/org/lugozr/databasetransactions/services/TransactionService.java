package main.java.org.lugozr.databasetransactions.services;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import main.java.org.lugozr.databasetransactions.exceptions.UnableToConnectException;
import main.java.org.lugozr.databasetransactions.models.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TransactionService {

    private static final String CONNECTION_ERROR = "Unable to get connection";
    private static final String PREPARED_STATEMENT_ERROR = "Prepared statement fail";

    private static final Logger logger = LogManager.getLogger(TransactionService.class.getName());

    private ConnectionService connectionService;
    public TransactionService(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public void deleteAllUsers(){
        String sql = "DELETE FROM users";
        try(Connection connection = connectionService.getConnection()){
            if(connection != null){
                connection.setAutoCommit(false);
                try(PreparedStatement preparedStatement = connection.prepareStatement(sql)){
                    preparedStatement.executeUpdate();
                    connection.commit();
                    connection.setAutoCommit(true);
                }catch (SQLException e){
                    connection.rollback();
                    logger.error(PREPARED_STATEMENT_ERROR);
                }
            }
        }catch (UnableToConnectException | SQLException e){
            logger.error(CONNECTION_ERROR);
        }
    }

    public List<User> findAllUsers() {
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM users ORDER BY username";
        try(Connection connection = connectionService.getConnection()) {
            if(connection != null) {
                connection.setAutoCommit(false);
                try(PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    ResultSet resultSet = preparedStatement.executeQuery()) {
                        connection.commit();
                        connection.setAutoCommit(true);
                        while (resultSet.next()) {
                            User user = new User();
                            user.setId(resultSet.getInt(1));
                            user.setUsername(resultSet.getString(2));
                            users.add(user);
                        }
                } catch (SQLException e){
                    connection.rollback();
                    logger.error(PREPARED_STATEMENT_ERROR);
                }
            }
        }catch (UnableToConnectException | SQLException e){
            logger.error(CONNECTION_ERROR);
        }
        return users;
    }

    public void insertUser(String username) {
        String sql1 = "SELECT username FROM users WHERE username=?";
        String sql2 = "INSERT INTO users (username) VALUES(?)";
        try(Connection connection = connectionService.getConnection()) {
            if(connection != null) {
                try (PreparedStatement preparedStatement1 = connection.prepareStatement(sql1);
                     PreparedStatement preparedStatement2 = connection.prepareStatement(sql2)) {
                    connection.setAutoCommit(false);
                    preparedStatement1.setString(1, username);
                    try(ResultSet resultSet = preparedStatement1.executeQuery()) {
                        if (!resultSet.first()) {
                            preparedStatement2.setString(1, username);
                            preparedStatement2.execute();
                        }
                    } catch (SQLException e){
                        logger.error("Result set fail");
                    }
                    connection.commit();
                    connection.setAutoCommit(true);
                } catch (SQLException e) {
                    connection.rollback();
                    logger.error(PREPARED_STATEMENT_ERROR);
                }
            }
        }catch (UnableToConnectException | SQLException e){
            logger.error(CONNECTION_ERROR);
        }
    }

    public void deleteUser(String username) {
        String sql = "DELETE FROM users WHERE username=?";
        try(Connection connection = connectionService.getConnection()){
            if(connection != null){
                connection.setAutoCommit(false);
                try(PreparedStatement preparedStatement = connection.prepareStatement(sql)){
                    preparedStatement.setString(1, username);
                    preparedStatement.executeUpdate();
                    connection.commit();
                    connection.setAutoCommit(true);
                }catch(SQLException e){
                    connection.rollback();
                    logger.error(PREPARED_STATEMENT_ERROR);
                }
            }
        }catch (UnableToConnectException | SQLException e){
            logger.error(CONNECTION_ERROR);
        }
    }

    public void updateUser(String oldUsername, String newUsername) {
        String sql = "UPDATE users SET username=? WHERE username=?";
        try(Connection connection = connectionService.getConnection()){
            if(connection != null){
                connection.setAutoCommit(false);
                try(PreparedStatement preparedStatement = connection.prepareStatement(sql)){
                    preparedStatement.setString(1, newUsername);
                    preparedStatement.setString(2, oldUsername);
                    preparedStatement.executeUpdate();
                    connection.commit();
                    connection.setAutoCommit(true);
                }catch (SQLException e){
                    connection.rollback();
                    logger.error(PREPARED_STATEMENT_ERROR);
                }
            }
        }catch (UnableToConnectException | SQLException e){
            logger.error(CONNECTION_ERROR);
        }
    }
}
