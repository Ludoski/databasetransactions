package main.java.org.lugozr.databasetransactions;

import main.java.org.lugozr.databasetransactions.models.User;
import main.java.org.lugozr.databasetransactions.services.ConnectionService;
import main.java.org.lugozr.databasetransactions.services.TransactionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Main{

    private static final Logger logger = LogManager.getLogger(Main.class.getName());

    public Main() {
        ConnectionService connectionService = new ConnectionService();
        TransactionService transactionService = new TransactionService(connectionService);
        transactionService.deleteAllUsers();
        for(int i = 0; i < 10; i++) {
            transactionService.insertUser("User" + i);
            transactionService.deleteUser("User" + (i-3));
            transactionService.updateUser("User" + (i-2), "User" + (i*i));
            printUsers(transactionService.findAllUsers());
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                logger.error("Thread unable to sleep");
            }
        }
    }

    public Main(String test){
        //Just for testing purpose
    }

    public void printUsers(List<User> data){
        if(!data.isEmpty()){
            System.out.println("-------------------------------------");
            System.out.println("Id" + "\t\t" + "Username");
            for(User user: data){
                System.out.println(user.getId() + "\t\t" + user.getUsername());
            }
            System.out.println("-------------------------------------");
        }
    }
}
