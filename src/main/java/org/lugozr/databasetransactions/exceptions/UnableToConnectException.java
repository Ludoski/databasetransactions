package main.java.org.lugozr.databasetransactions.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UnableToConnectException extends Exception {

    private static final Logger logger = LogManager.getLogger(UnableToConnectException.class.getName());

    public UnableToConnectException(Exception e){
        logException(e);
    }

    public void logException(Exception e){
        logger.error(e.getMessage());
        logger.error(e.getClass().getCanonicalName());
        logger.error(e.getStackTrace()[0].getLineNumber());
        logger.error(e.getStackTrace()[0].getClassName());
        logger.error(e.getStackTrace()[0].getMethodName());
        logger.info("------------------------------------");
    }
}
