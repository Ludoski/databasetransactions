package test.java.org.lugozr.databasetransactions.services;

import main.java.org.lugozr.databasetransactions.exceptions.UnableToConnectException;
import main.java.org.lugozr.databasetransactions.services.ConnectionService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TransactionServiceTest {

    Connection connection;

    @Before
    public void getConnection(){
        ConnectionService connectionService = new ConnectionService();
        try {
            connection = connectionService.getConnection();
        } catch (UnableToConnectException e) {
            e.printStackTrace();
        }
    }

    @After
    public void closeConnection(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void closeStatementShouldCloseStatement() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("");
            preparedStatement.close();
            assertTrue(preparedStatement.isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void closeStatementWithNullShouldNotThrow() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("");
            preparedStatement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
