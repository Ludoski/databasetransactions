package test.java.org.lugozr.databasetransactions;

import main.java.org.lugozr.databasetransactions.Main;
import main.java.org.lugozr.databasetransactions.models.User;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class MainTest {

    public List<User> userList = new ArrayList<>();

    @Before
    public void init(){
        for(int i = 0; i < 10; i++){
            User user = new User();
            user.setId(i);
            user.setUsername("User" + i);
            userList.add(user);
        }
    }

    @Test
    public void printUsers(){
        Main main = new Main("test");
        main.printUsers(userList);
        assertEquals(userList.size(), 10);
    }
}
